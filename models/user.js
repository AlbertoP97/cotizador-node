const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

/* Creating data schema for mapping */
const schema = mongoose.Schema({
    _name: { // Name attribute for users schema
        type: String,
        trim: true,
        required: true,  // Name attribute is required
        maxLength: 25 // User first name max size
    },
    _lastName: { // Last name attribute for users schema
        type: String,
        trim: true,
        required: true,  // Last name attribute is required
        maxLength: 50 // User last name max size
    },
    _email: { // Name attribute for users schema
        type: String,
        trim: true,
        required: true, // Email attribute is required
        unique: 32
    },
    _password: { // Name attribute for users schema
        type: String,
        required: true, // Password attribute is required
    },
    _salt: String
});

/* Creating schema class with its encapsulated attributes */
class User {

    /* Class constructor with its attributes */
    constructor(name, lastName, email, password, salt){
        this._name = name;
        this._lastName = lastName;
        this._email = email;
        this._password = password;
        this._salt = salt;
    }

    /* Get method to get the attribute user name */
    get name(){
        return this._name;
    }

    /* Set method to modify attribute user name */
    set name(v){
        this._name = v;
    }

    /* Get method to get the attribute user last name */
    get lastName(){
        return this._lastName;
    }

    /* Set method to modify attribute user last name */
    set lastName(v){
        this._lastName = v;
    }
    
    /* Get method to get the attribute email */
    get email(){
        return this._email;
    }

    /* Set method to modify attribute email */
    set email(v){
        this._email = v;
    }

    /* Get method to get the attribute password */
    get password(){
        return this._password;
    }

    /* Set method to modify attribute password */
    set password(v){
        this._password = v;
    }
    
    /* Get method to get the attribute userName */
    get salt(){
        return this._salt;
    }

    /* Set method to modify attribute userName */
    set salt(v){
        this._salt = v;
    }
}

/* Loading class from schema defined for conversion to object */
schema.loadClass(User); 
schema.plugin(mongoosePaginate);
/* Exporting the schema */
module.exports = mongoose.model('User', schema);