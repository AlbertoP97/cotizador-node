const mongoose = require('mongoose');

/* Creating data schema for mapping */
const schema = mongoose.Schema({
    _name: { // Name attribute for product schema
        type: String,
        trim: true,
        required: true, // Name attribute is required
        maxLength: 4000 // Product name max size
    },
    _description: { // Description attribute for product schema
        type: String,
        maxLength: 4000 // Product description max size
    },
    _price: { // Price attribute for product schema
        type: Number,
        required: true // Price attribute is required
    },
});

/* Creating schema class with its encapsulated attributes */
class Product {

    /* Class constructor with its attributes */
    constructor(name, description, price){
        this._name = name;
        this._description = description;
        this._price = price;
    }

    /* Get method to get the attribute product name */
    get name(){
        return this._name;
    }

    /* Set method to modify attribute product name */
    set name(v){
        this._name = v;
    }

    /* Get method to get the attribute product description */
    get description(){
        return this._description;
    }

    /* Set method to modify attribute product description */
    set description(v){
        this._description = v;
    }
    
    /* Get method to get the attribute product price */
    get price(){
        return this._price;
    }

    /* Set method to modify attribute product price */
    set price(v){
        this._price = v;
    }
}

/* Loading class from schema defined for conversion to object */
schema.loadClass(Product); 
module.exports = mongoose.model('Product', schema);