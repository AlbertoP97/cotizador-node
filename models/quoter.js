const mongoose = require('mongoose');

/* Creating data schema for mapping */
const schema = mongoose.Schema({
    _quoteNumber: { // Number of quoters
        type: Number,
        default: 0
    },
	_products:[{productId: String, number: Number}]
});

/* Creating schema class with its encapsulated attributes */
class Quoter {

    /* Class constructor with its attributes */
    constructor(quoteNumber){
        this._quoteNumber = quoteNumber;
    }

    /* Get method to get the attribute quoter's number */
    get quoteNumber(){
        return this._quoteNumber;
    }

    /* Set method to modify attribute quoter's number */
    set quoteNumber(v){
        this._quoteNumber = v;
    }
	
	/* Get method to get the attribute quoter's products */
	get products(){
		return this._products;
	}
	
	/* Set method to modify attribute quoter's products */
	set products(v){
        this._products = v;
    }
}

/* Loading class from schema defined for conversion to object */
schema.loadClass(Quoter); 
module.exports = mongoose.model('Quoter', schema);