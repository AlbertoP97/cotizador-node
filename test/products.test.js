const supertest = require('supertest');
const app = require('../app');

var key = "";

// Probando el POST de los productos
describe('Probar la ruta POST de los productos', () => {
    it('Deberia de crear a un nuevo producto', (done) => {
        supertest(app).post('/products/blank')
        .send({'name': 'Celular', 'price': '3500.99'})
        .expect(200)
        .end(function(err, res) {
            key = res.body.objs;
            done();
        });
    });
});

// Probando el GET de los productos
describe('Probar las rutas de los productos', () => {
    it('Deberia de obtener la lista de productos', (done) => {
        supertest(app).get('/products/')
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res) {
            if(err) {
                done(err);
            } else {
                //expect(res.body.objs.limit).toEqual(5);
                expect(res.statusCode).toEqual(200);
                done();
            }
        });
    });
});

// Probando el GET de un producto especifico
describe('Probar la ruta GET de un producto en especifico', () => {
    it('Deberia de obtener al producto especificado', (done) => {
        supertest(app).get('/products/edit/628718f7b122b0e7ba02ed3f')
        .set('Accept', `Bearer ${key}`)
        .end(function(err, res) {
            if(err) {
                done(err);
            } else {
                expect(res.statusCode).toEqual(200);
                done();
            }
        });
    });
});

// Probando el DELETE de un producto especifico
describe('Probar la ruta DELETE de un producto en especifico', () => {
    it('Deberia de borrar al producto especificado', (done) => {
        supertest(app).get('/products/:id')
        .send({'name': 'Celular', 'price': '3500.99'})
        .set('Accept', `Bearer ${key}`)
        .end(function(err, res) {
            if(err) {
                done(err);
            } else {
                expect(res.statusCode).toEqual(200);
                done();
            }
        });
    });
});