const supertest = require('supertest');
const app = require('../app');

var key = "";
// Sentence
describe('Probar sistema de autenticacion', () => {
    // Test cases => 50% (one test that succeeds and one that fails)
    it('Deberia tener un usuario y contrasena correctos', (done) => {
        supertest(app).post('/login')
        .send({'email': 'albertopm@gmail.com', 'password': 'abcde'})
        .expect(200)
        .end(function(err, res) {
            key = res.body.objs;
            done();
        });
    });
});

// Probando el POST de los usuarios
describe('Probar la ruta POST de los usuarios', () => {
    it('Deberia de crear a un nuevo usuario', (done) => {
        supertest(app).post('/users/blank')
        .send({'name': 'Gerardo', 'lastName': 'Barraza', 'email': 'gerardo@gmail.com', 'password': 'abcde'})
        .expect(200)
        .end(function(err, res) {
            key = res.body.objs;
            done();
        });
    });
});

// Probando el GET de los usuarios
describe('Probar las rutas de los usuarios', () => {
    it('Deberia de obtener la lista de usuarios', (done) => {
        supertest(app).get('/users/')
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res) {
            if(err) {
                done(err);
            } else {
                //expect(res.body.objs.limit).toEqual(5);
                expect(res.statusCode).toEqual(200);
                done();
            }
        });
    });
});

// Probando el GET de un usuario especifico
describe('Probar la ruta GET de un usuario en especifico', () => {
    it('Deberia de obtener al usuario especificado', (done) => {
        supertest(app).get('/users/edit/627ebcfc06c45dd07897fd72')
        .set('Accept', `Bearer ${key}`)
        .end(function(err, res) {
            if(err) {
                done(err);
            } else {
                expect(res.statusCode).toEqual(200);
                done();
            }
        });
    });
});

// Probando el DELETE de un usuario especifico
describe('Probar la ruta DELETE de un usuario en especifico', () => {
    it('Deberia de borrar al usuario especificado', (done) => {
        supertest(app).get('/users/:id')
        .send({'name': 'Gerardo'})
        .set('Accept', `Bearer ${key}`)
        .end(function(err, res) {
            if(err) {
                done(err);
            } else {
                expect(res.statusCode).toEqual(200);
                done();
            }
        });
    });
});