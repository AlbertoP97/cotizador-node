const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const { expressjwt: jwt } = require('express-jwt');
const config = require('config');
const i18n = require('i18n');
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const productsRouter = require('./routes/products');
const quotersRouter = require('./routes/quoters');
const mongoose = require('mongoose');
const methodOverride = require('method-override');

const jwtKey = config.get("secret.key"); /* Jsonwebtoken hash obtained from the default environment configuration file */

const app = express();


/* URI from mongodb atlas*/
const uri = config.get('dbChain'); // Connection to the db in production environment

mongoose.connect(uri); // Connecting to mongodb uri

const db = mongoose.connection; // Creating the connection to db

// Showing message of error connection to the db
db.on('error', () =>{
  console.log("No se ha conectado a la base de datos");
});

// Showing message of successful connection to the db
db.on('open', () =>{
  console.log("Se ha conectado a la base de datos");
});

// Configuring languages
i18n.configure({
  locales: ['es', 'en'],
  cookie: 'language',
  directory: `${__dirname}/locales`
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/modules', express.static(__dirname + '/node_modules/'));
app.use(i18n.init);
app.use(methodOverride('_method'));

//app.use(jwt({secret: jwtKey, algorithms: ['HS256']}) // Middleware that is responsible for user authentication in the system
//  .unless({path:["/login", "/"]}));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/products', productsRouter);
app.use('/quoters', quotersRouter);




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
