window.jsPDF = window.jspdf.jsPDF;
const doc = new jsPDF();
var htmlForm = document.querySelector('#modal');

function MakePDF() {
    console.log('Creando pdf');
    doc.html(htmlForm, {
        callback: function (doc) {
            doc.output('dataurlnewwindow', {filename: 'Cotizador.pdf'});
        },
        x: 15,
        y: 15,
        width: 170,
        windowWidth: 650
    });
}