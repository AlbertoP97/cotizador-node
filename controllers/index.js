const express = require('express');
const bcrypt = require('bcrypt');
const async = require('async');
const jwt = require('jsonwebtoken');
const config = require('config');

const User = require('../models/user'); // Importing the users schema
const jwtKey = config.get("secret.key"); /* Jsonwebtoken hash obtained from the default environment configuration file*/

/* Function that render home page */
function home(req, res, next) {
    res.render('index', { title: 'Express', password: false });
}

function dashboard(req, res, next) {
    res.render('home');
}

/* Function that logs an user*/
function login(req, res, next){
    let email = req.body.email;
    let password = req.body.password;

    console.log(`${email} --- ${password}`);

    /* Parallel function that verify user credentials */
    async.parallel({
        user : callback => User.findOne({_email:email}) // Query for user email
        .select('_password _salt') // Query for user password
        .exec(callback) // Execute callback function
    }, (err, result) => {
        if(result.user){
            bcrypt.hash(password, result.user.salt, (err, hash) => { // Check if the hash matches the user password
                if(hash === result.user.password) { // If they are equal, the user is authenticated
                    res.redirect('/home');
                } else {
                    res.render('index', {title: 'Express', password: true}); // If they're not equal, the user cannot be authenticated
                }
            });
        } else {
            res.render('index', {title: 'Express', password: true}); // If the email is not found, the user cannot be authenticated
        }
    });
}

module.exports = {
    home, dashboard, login
}