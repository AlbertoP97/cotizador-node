const express = require('express');
const config = require('config');

const Product = require('../models/product'); // Calling product schema

function blank(req, res, next) {
    res.render('products/blank');
}

/* Function that creates a new product in the system */
function create(req, res, next) {
    let name = req.body.name; // Creating the user product name variable
    let description = req.body.description; // Creating the product description variable
    let price = req.body.price; // Creating the product price variable

    // Product's constructor
    let product = new Product({
        name: name,
        description: description,
        price: price,
    });
            
    product.save().then(obj => res.redirect('/products/home')) // Redirect to home page when new product was created
    .catch(err => res.status(500).json({
        message: res.__('error.adding'),
        objs: err
    }));
}

/* Function that shows all the products of the system */
function list(req, res, next) {
    let page = req.params.page ? req.params.page : 1;
    Product.find().then((objs) => { 
        console.log(objs);
        res.render('products/list', {products: objs}); // Show all products int he system
    }).catch(err => res.status(500).json({
        message: res.__('error.list'),
        objs: err
    }));
}

/* Function that shows a specific product in the system */
function index(req, res, next) {
    const id = req.params.id; // Variable that saves the product id
    Product.findOne({"_id":id}).then(obj => res.status(200).json({
        message: `Producto del sistema con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: `No se pudo encontrar el producto del sistema con id ${id}`,
        objs: err
    }));
}

/* Function that render and edits a specific product in the system */
function show(req, res, next) {
    const id = req.params.id;
    Product.findOne({"_id":id}).then(obj => res.render('products/edit', {product:obj})) // Edit a specific product whit the id reference
    .catch(err => res.status(500).json({
        message: `No se pudo encontrar el producto del sistema con id ${id}`,
        objs: err
    }));
}

/* Function that edits a specific product in the system */
function edit(req, res, next) {
    const id = req.params.id; // Variable that saves the product id
    let name = req.body.name; // Variable that saves the product name
    let description = req.body.description // Variable that saves the product description
    let price = req.body.price; // Variable that saves the product price

    let product = new Object(); // Create a new object 

    /* Compare if the product name you want to modify is the same as the one previously saved */
    if(name){
        product._name = name;
    }

    /* Compare if the product description you want to modify is the same as the one previously saved */
    if(description){
        product._description = description;
    }

    /* Compare if the product price you want to modify is the same as the one previously saved */
    if(price){
        product._price = price;
    }

    // Showing the modified product
    Product.findOneAndUpdate({"_id":id}, product).then(obj => res.redirect('/products/home'))
    .catch(err => res.status(500).json({
        message: `No se pudieron modificar los atributos del producto del sistema con id ${id}`,
        objs: err
    }));
}

/* Function that replaces a specific product in the system */
function replace(req, res, next) {
    const id = req.params.id; // Variable that saves the product id
    let name = req.body.name ? req.body.name : ""; // It is verified if the variables have information saved, if not, the variable is left blank
    let description = req.body.description ? req.body.description : ""; // It is verified if the variables have information saved, if not, the variable is left blank
    let price = req.body.price ? req.body.price : ""; // It is verified if the variables have information saved, if not, the variable is left blank
   
    // Calling the product's constructor again with the new parameters
    let product = new Object({
        _name: name,
        _description: description,
        _price: price
    });

    /* Finding the product and replacing it with the new data captured above */
    Product.findOneAndReplace({"_id":id}, product).then(obj => res.status(200).json({
        message: `Se reemplazo el producto con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: `No se puede reemplazar el producto con id ${id}`,
        objs: err
    }));
}

/* Function that deletes a specific product in the system */
function destroy(req, res, next) {
    const id = req.params.id; // Variable that saves the product id
    Product.remove({"_id":id}).then(obj => res.redirect('/products/home'))
    .catch(err => res.status(500).json({
        message: `No se puedo eliminar el producto con id ${id}`,
        objs: err
    }));;
}

function find(req, res, next) {
    console.log(req.params.name);
    Product.findOne({ name: req.params.name }).then(obj => res.json({
        name: obj._name,
        description: obj._description,
        price: obj._price
    }))
        .catch(err => res.status(500).json({
            message: `No se pudo encontrar el producto del sistema con nombre ${req.params.name}`,
            objs: err
        }));
}

module.exports = {
    blank, create, list, index, show, edit, replace, destroy, find
}