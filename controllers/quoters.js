const express = require('express');
const config = require('config');
const sendQuote = require('./email'); // Calling sendGrid controller

const Quoter = require('../models/quoter'); // Calling quoter schema

function blank(req, res, next) {
    res.render('quoters/blank');
}

/* Function that creates a new quoter in the system */
function create(req, res, next) {
    let quoteNumber = req.body.quoteNumber; // Creating the user quoter number variable

    // Quoter's constructor
    let quoter = new Quoter({
        quoteNumber: quoteNumber
    });
            
    quoter.save().then(obj => res.redirect('/quoters/home')) // Redirect to the home page when the new quote was created
    .catch(err => res.status(500).json({
        message: res.__('error.adding'),
        objs: err
    }));
}

/* Function that shows all the quoters of the system */
function list(req, res, next) {
    let page = req.params.page ? req.params.page : 1;
    Quoter.find().then((objs) => { 
        console.log(objs);
        res.render('quoters/list', {quoters: objs}); // Show all quoters
    }).catch(err => res.status(500).json({
        message: res.__('error.list'),
        objs: err
    }));
}

/* Function that shows a specific quoter in the system */
function index(req, res, next) {
    const id = req.params.id; // Variable that saves the quoter id
    Quoter.findOne({"_id":id}).then(obj => res.status(200).json({
        message: `Cotizacion con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: `No se pudo encontrar la cotizacion con id ${id}`,
        objs: err
    }));
}

/* Function that render and edits a specific quoter in the system */
function show(req, res, next) {
    const id = req.params.id;
    Quoter.findOne({"_id":id}).then(obj => res.render('quoters/edit', {quoter:obj}))
    .catch(err => res.status(500).json({
        message: `No se pudo encontrar la cotizacion con id ${id}`,
        objs: err
    }));
}

/* Function that edits a specific quoter in the system */
function edit(req, res, next) {
    const id = req.params.id; // Variable that saves the quoter id
    let quoteNumber = req.body.quoteNumber; // Variable that saves the quoter name
	let products = req.body.products;// Variable that saves the quoter products
    
    let quoter = new Object(); // Create a new object 

    /* Compare if the quoter's number you want to modify is the same as the one previously saved */
    if(quoteNumber){
        quoter._quoteNumber = quoteNumber;
    }

	/* Compare if the quoter's products you want to modify is the same as the one previously saved */
    if(products){
        quoter._products = products;
    }

    // Showing the modified product
    Quoter.findOneAndUpdate({"_id":id}, quoter).then(obj => res.redirect('/quoters/home'))
    .catch(err => res.status(500).json({
        message: `No se pudieron modificar los atributos de la cotizacion con id ${id}`,
        objs: err
    }));
}

/* Function that replaces a specific quoter in the system */
function replace(req, res, next) {
    const id = req.params.id; // Variable that saves the quoter id
    let quoteNumber = req.body.quoteNumber ? req.body.quoteNumber : ""; // It is verified if the variables have information saved, if not, the variable is left blank
    
    // Calling the quoter's constructor again with the new parameters
    let quoter = new Object({
        _quoteNumber: quoteNumber
    });

    /* Finding the quoter and replacing it with the new data captured above */
    Quoter.findOneAndReplace({"_id":id}, quoter).then(obj => res.status(200).json({
        message: `Se reemplazo la cotizacion con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: `No se puede reemplazar la cotizacion con id ${id}`,
        objs: err
    }));
}

/* Function that deletes a specific quoter in the system */
function destroy(req, res, next) {
    const id = req.params.id; // Variable that saves the quoter id
    Quoter.remove({"_id":id}).then(obj => res.redirect('/quoters/home'))
    .catch(err => res.status(500).json({
        message: `No se puedo eliminar la cotizacion con id ${id}`,
        objs: err
    }));;
}

module.exports = {
    blank, create, list, index, show, edit, replace, destroy
}