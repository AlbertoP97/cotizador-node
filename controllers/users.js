const express = require('express');
const bcrypt = require('bcrypt');
const async = require('async');
const config = require('config');

const User = require('../models/user'); // Calling user schema

function blank(req, res, next) {
    res.render('users/blank');
}

/* Function that creates a new user in the system */
function create(req, res, next) {
    let name = req.body.name; // Creating the user user name variable
    let lastName = req.body.lastName; // Creating the user user last name variable
    let email = req.body.email; // Creating the user email variable
    let password = req.body.password; // Creating the user password variable

    // Parallel function that allows us to create system users
    async.parallel({
        salt:(callback) => {
            bcrypt.genSalt(10, callback);
        }
    }, (err, result) => {
        bcrypt.hash(password, result.salt, (err, hash) => { // Function that encrypts the password of the users

            // User's constructor
            let user = new User({
                name: name,
                lastName: lastName,
                email: email,
                password: hash,
                _salt: result.salt
            });
            
            user.save().then(obj => res.redirect('/users/home')) // Redirect to the main page when the new user was created
            .catch(err => res.status(500).json({
                message: res.__('error.register'),
                objs: err
            }));
        });
    });
}

/* Function that shows all the users of the system */
function list(req, res, next) {
    let page = req.params.page ? req.params.page : 1; // Variable that allows obtaining the page number if it is required
    User.paginate({}, {page:page, limit:config.get('paginate.size')}) // Paginate users in different pages
    .then((objs) => { 
        console.log(objs);
        res.render('users/list', {users: objs}); // Show all users in a specific page
    }).catch(err => res.status(500).json({
        message: res.__('error.list'),
        objs: err
    }));
}

/* Function that shows a specific user in the system */
function index(req, res, next) {
    const id = req.params.id; // Variable that saves the user id
    User.findOne({"_id":id}).then(obj => res.status(200).json({ // Shows the attributes of a specific user
        message: `Usuario del sistema con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: `No se pudo encontrar el usuario del sistema con id ${id}`,
        objs: err
    }));
}

/* Function that render a specific user in the system */
function show(req, res, next) {
    const id = req.params.id;
    User.findOne({"_id":id}).then(obj => res.render('users/edit', {user:obj})) // Render the attributes of a specific user
    .catch(err => res.status(500).json({
        message: `No se pudo encontrar el usuario del sistema con id ${id}`,
        objs: err
    }));
}

/* Function that edits a specific user in the system */
function edit(req, res, next) {
    const id = req.params.id; // Variable that saves the user id
    let name = req.body.name; // Variable that saves the user name
    let lastName = req.body.lastName; // Variable that saves the user last name
    let email = req.body.email; // Variable that saves the user email

    let user = new Object(); // Create a new object 

    /* Compare if the user name you want to modify is the same as the one previously saved */
    if(name){
        user._name = name;
    }

    /* Compare if the user last name you want to modify is the same as the one previously saved */
    if(lastName){
        user._lastName = lastName;
    }

    /* Compare if the user email you want to modify is the same as the one previously saved */
    if(email){
        user._email = email;
    }

    // Showing the modified user
    User.findOneAndUpdate({"_id":id}, user).then(obj => res.redirect('/users/home'))
    .catch(err => res.status(500).json({
        message: `No se pudieron modificar los atributos del usuario del sistema con id ${id}`,
        objs: err
    }));
}

/* Function that replaces a specific user in the system */
function replace(req, res, next) {
    const id = req.params.id; // Variable that saves the user id
    let name = req.body.name ? req.body.name : ""; // It is verified if the variables have information saved, if not, the variable is left blank
    let lastName = req.body.lastName ? req.body.lastName : ""; // It is verified if the variables have information saved, if not, the variable is left blank
    let email = req.body.email ? req.body.email : ""; // It is verified if the variables have information saved, if not, the variable is left blank
    let password = req.body.password ? req.body.password : ""; // It is verified if the variables have information saved, if not, the variable is left blank

    // Calling the user's constructor again with the new parameters
    let user = new Object({
        _name: name,
        _lastName: lastName,
        _email: email,
        _password: password
    });

    /* Finding the user and replacing it with the new data captured above */
    User.findOneAndReplace({"_id":id}, user).then(obj => res.status(200).json({
        message: `Se reemplazo el usuario con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: `No se puede reemplazar el usuario con id ${id}`,
        objs: err
    }));
}

/* Function that deletes a specific user in the system */
function destroy(req, res, next) {
    const id = req.params.id; // Variable that saves the user id
    User.remove({"_id":id}).then(obj => res.redirect('/users/home')) // Redirect to home page when user was deleted
    .catch(err => res.status(500).json({
        message: `No se puedo eliminar el usuario con id ${id}`,
        objs: err
    }));;
}

module.exports = {
    blank, create, list, index, show, edit, replace, destroy
}