const express = require('express');
const controller = require('../controllers/products');
const router = express.Router();

/* POST products creating. */
router.post('/', controller.create);

/* GET products creating. */
router.get('/blank', controller.blank);

/* GET specific product editing. */
router.get('/edit/:id', controller.show);

/* GET products listing. */
router.get('/:page?', controller.list);

/* GET specific product listing. */
router.get('/show/:id', controller.index);

/* PUT specific product editing. */
router.put('/:id', controller.replace);

/* PATCH specific product replacing. */
router.patch('/:id', controller.edit);

/* DELETE specific product deleting. */
router.delete('/:id', controller.destroy);

/* FIND first product that has the name. */
router.get('/find/:name', controller.find);

module.exports = router;
