const express = require('express');
const controller = require('../controllers/users');
const router = express.Router();

/* POST users creating. */
router.post('/', controller.create);

/* GET users creating. */
router.get('/blank', controller.blank);

/* GET specific user editing. */
router.get('/edit/:id', controller.show);

/* GET users listing. */
router.get('/:page?', controller.list);

/* GET specific user listing. */
router.get('/show/:id', controller.index);

/* PUT specific user editing. */
router.put('/:id', controller.replace);

/* PATCH specific user replacing. */
router.patch('/:id', controller.edit);

/* DELETE specific user deleting. */
router.delete('/:id', controller.destroy);

module.exports = router;
