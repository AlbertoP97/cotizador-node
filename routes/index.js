const express = require('express');
const controller = require('../controllers/index');
const router = express.Router();

/* GET home page. */
router.get('/', controller.home);

/* POST login users. */
router.post('/login', controller.login);

/* GET home page */ 
router.get('/home', controller.dashboard);

module.exports = router;
