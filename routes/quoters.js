const express = require('express');
const controller = require('../controllers/quoters');
const router = express.Router();

/* POST quoters creating. */
router.post('/', controller.create);

/* GET quoters creating. */
router.get('/blank', controller.blank);

/* Get*/

/* GET specific quoter editing. */
router.get('/edit/:id', controller.show);

/* GET quoters listing. */
router.get('/:page?', controller.list);

/* GET specific quoter listing. */
router.get('/show/:id', controller.index);

/* PUT specific quoter editing. */
router.put('/:id', controller.replace);

/* PATCH specific quoter replacing. */
router.patch('/:id', controller.edit);

/* DELETE specific quoter deleting. */
router.delete('/:id', controller.destroy);

module.exports = router;